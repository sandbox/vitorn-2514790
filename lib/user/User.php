<?php

/**
 * Description of Menu
 * This classes represents the 'user' entity on Drupal 7
 * Instead to access the arrays of the entity, use this helpers function to access data
 * $user entity (https://api.drupal.org/api/drupal/developer!globals.php/global/user/7)
 * @author vitorn
 */
class User {

  // Properties
  private $uid = NULL; // User ID
  private $drupal_user; // Drupal original entity
  private $is_anonymous = NULL; // TRUE if user is anonymous
  private $name = NULL; // - User name
  private $pass = NULL; // - Encrypted password
  private $mail = NULL; // - User current email address
  private $theme = NULL; // - Name of the theme shown for this user (no longer changeable in core but can be in contrib)
  private $signature = NULL; // - Signature as set in the user account settings
  private $signature_format = NULL; // - Text format to apply to signature
  private $created = NULL; // - Unix timstamp for when the account was created
  private $access = NULL; // - Unix timstamp of the last time the user accessed the site
  private $login = NULL; // - Unix timstamp of the last successful login by this user
  private $status = NULL; // - 1 if the user is active, 0 if blocked
  private $timezone = NULL; // - User's timezone as a PHP compatible timezone string ( date_default_timezone_set() )
  private $language = NULL; // - User's language code
  private $picture = NULL; // - User picture / avatar
  private $init = NULL; // - Contains the email address the user provided during initial registration
  private $data = NULL; // - Data stored in the users table by contrib modules (second argument of user_save())
  private $sid = NULL; // - Session ID for HTTP sessions
  private $ssid = NULL; // - Session ID for HTTPS sessions
  private $hostname = NULL; // - User's IP address
  private $timestamp = NULL; // - A timestamp of last user access
  private $cache = NULL; // - A timestamp used in cache.inc to check freshness of cached data
  private $session = NULL; // - Variables stored in the session through $_SESSION
  private $roles = NULL; // Roles of the user
  private $custom_fields = NULL; // Array with the custom fields of the User

  // Constructor

  public function __construct($uid = 0) {
    $this->uid = $uid;

    // If uid is 0 , is anonymous
    if ($this->uid == 0) {
      $this->is_anonymous = TRUE;
    }

    // Load the user
    $this->drupal_user = user_load($uid);
    
    // Parse the user info
    $this->parse();
  }

  // Function to parse the User info
  private function parse() {
    // Copy the values from the entity user to this class
    if (isset($this->drupal_user->name) && !empty($this->drupal_user->name)) {
      $this->name = $this->drupal_user->name;
    }

    if (isset($this->drupal_user->pass) && !empty($this->drupal_user->pass)) {
      $this->pass = $this->drupal_user->pass;
    }

    if (isset($this->drupal_user->mail) && !empty($this->drupal_user->mail)) {
      $this->mail = $this->drupal_user->mail;
    }

    if (isset($this->drupal_user->theme) && !empty($this->drupal_user->theme)) {
      $this->theme = $this->drupal_user->theme;
    }

    if (isset($this->drupal_user->signature) && !empty($this->drupal_user->signature)) {
      $this->signature = $this->drupal_user->signature;
    }

    if (isset($this->drupal_user->signature_format) && !empty($this->drupal_user->signature_format)) {
      $this->signature_format = $this->drupal_user->signature_format;
    }

    if (isset($this->drupal_user->created) && !empty($this->drupal_user->created)) {
      $this->created = $this->drupal_user->created;
    }

    if (isset($this->drupal_user->access) && !empty($this->drupal_user->access)) {
      $this->access = $this->drupal_user->access;
    }

    if (isset($this->drupal_user->login) && !empty($this->drupal_user->login)) {
      $this->login = $this->drupal_user->login;
    }

    if (isset($this->drupal_user->name) && !empty($this->drupal_user->name)) {
      $this->status = $this->drupal_user->status;
    }

    if (isset($this->drupal_user->timezone) && !empty($this->drupal_user->timezone)) {
      $this->timezone = $this->drupal_user->timezone;
    }

    if (isset($this->drupal_user->language) && !empty($this->drupal_user->language)) {
      $this->language = $this->drupal_user->language;
    }

    if (isset($this->drupal_user->picture) && !empty($this->drupal_user->picture)) {
      $this->picture = $this->drupal_user->picture;
    }

    if (isset($this->drupal_user->init) && !empty($this->drupal_user->init)) {
      $this->init = $this->drupal_user->init;
    }

    if (isset($this->drupal_user->data) && !empty($this->drupal_user->data)) {
      $this->data = $this->drupal_user->data;
    }

    if (isset($this->drupal_user->sid) && !empty($this->drupal_user->sid)) {
      $this->sid = $this->drupal_user->sid;
    }

    if (isset($this->drupal_user->ssid) && !empty($this->drupal_user->ssid)) {
      $this->ssid = $this->drupal_user->ssid;
    }

    if (isset($this->drupal_user->hostname) && !empty($this->drupal_user->hostname)) {
      $this->hostname = $this->drupal_user->hostname;
    }

    if (isset($this->drupal_user->timestamp) && !empty($this->drupal_user->timestamp)) {
      $this->timestamp = $this->drupal_user->timestamp;
    }

    if (isset($this->drupal_user->cache) && !empty($this->drupal_user->cache)) {
      $this->cache = $this->drupal_user->cache;
    }

    if (isset($this->drupal_user->cache) && !empty($this->drupal_user->cache)) {
      $this->session = $this->drupal_user->session;
    }
    $this->roles = $this->drupal_user->roles;

    // Parse custom fields
    $this->parse_custom_fields();
  }

  /** Function to process the custon field of the user entity */
  private function parse_custom_fields() {
    if ($this->uid == 0) {
      return FALSE;
    }
    // Initialize custon fields
    $this->custom_fields = array();

    // Get the fields
    $user_entity_fields = field_info_instances('user', 'user');

    // Make a loop passing by all arrays and get the field machine name
    foreach ($user_entity_fields as $field_entity) {
      // Get the name
      $field_name = $field_entity['field_name'];
      
      // Get the language of the field
      $field_lang = field_language('user', $this->drupal_user, $field_name);
      
      // And attr the values
      $field = $this->drupal_user->${field_name};
      $this->custom_fields[$field_name] = $field[$field_lang][0]['value'];
    }
  }
  
  /** Function to get a value from the field */
  public function getFieldValue($field_name) {
    return $this->custom_fields[$field_name];
  }
  
  /** Function to get the array with all custom field values */
  public function getAllFieldValues() {
    return $this->custom_fields;
  }

  /** Static function will return a instance of this class with the user loaded */
  public static function load($uid) {
    return new User($uid);
  }

  /** Function to return the current user */
  public static function getCurrentUser() {
    global $user;
    return new User($user->uid);
  }

  /** Function to know if the current user is logged in */
  public static function isLoggedIn() {
    return user_is_logged_in();
  }

}
