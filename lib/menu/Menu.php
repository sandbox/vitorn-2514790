<?php

/**
 * Description of Menu
 * This classes represents the 'menu' array on Drupal 7
 * hook_menu (https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7)
 * @author vitorn
 */
// Define the class
class Menu {

  // Define two constants as possible values for $position member
  const POSITION_RIGHT = 'right';
  const POSITION_LEFT = 'left';

  // All members of menu
  private $item = NULL; // This is the array with the menu option
  private $url = NULL; // The URL of the menu
  private $title = NULL; //  The untranslated title of the menu item.
  private $title_callback = NULL; //  Function to generate the title; defaults to t(). If you require only the raw string to be output, set this to FALSE.
  private $title_arguments = NULL; // Arguments to send to t() or your custom callback, with path component substitution as described above.
  private $delivery_callback = NULL; // The function to call to package the result of the page callback function and send it to the browser.
  private $description = NULL; // The untranslated description of the menu item.
  private $access_callback = NULL; // A function returning TRUE if the user has access rights to this menu item, and FALSE if not. 
  private $access_arguments = NULL; //  An array of arguments to pass to the access callback function,
  private $page_arguments = NULL; //  An array of arguments to pass to the page callback function, with path component substitution
  private $page_callback = NULL; // The function to call to display a web page when the user visits the path.
  private $theme_callback = NULL; // A function returning the machine-readable name of the theme that will be used to render the page
  private $theme_arguments = NULL; //An array of arguments to pass to the access callback function, with path component substitution
  private $file = NULL; // A file that will be included before the page callback is called
  private $file_path = NULL; // The path to the directory containing the file specified in "file"
  private $load_arguments = NULL; //  An array of arguments to be passed to each of the wildcard object loaders in the path, after the path argument itself
  private $weight = NULL; //  An integer that determines the relative position of items in the menu
  private $menu_name = NULL; // Set this to a custom menu if you don't want your item to be placed in Navigation.
  private $expanded = NULL; // If set to TRUE, and if a menu link is provided for this menu item
  private $context = NULL; // Defines the context a tab may appear in.
  private $position = NULL; // Position of the block
  private $tab_parent = NULL; //  For local task menu items, the path of the task's parent item
  private $tab_root = NULL; // For local task menu items, the path of the closest non-tab item
  private $type = NULL; //  A bitmask of flags describing properties of the menu item
  private $options = NULL; // An array of options to be passed to l() when generating a link from this menu item.

  // Methods

  public function __construct($url) {
    $this->url = $url;
  }

  // This funciton will create the array understandable for Drupal
  private function render() {
    // Init item
    $this->item[$this->url] = array();

    // Now start to check the attrs, if they are there and setting on $item
    // Check title, title callback and title arguments
    if (!empty($this->title)) {
      $this->item[$this->url]['title'] = $this->title;
    }

    if (!empty($this->title_callback)) {
      $this->item[$this->url]['title callback'] = $this->title_callback;
    }

    if (!empty($this->title_arguments)) {
      $this->item[$this->url]['title arguments'] = $this->title_arguments;
    }

    // Check description
    if (!empty($this->description)) {
      $this->item[$this->url]['description'] = $this->description;
    }

    // Check Page callback and page arguments
    if (!empty($this->page_callback)) {
      $this->item[$this->url]['page callback'] = $this->page_callback;
    }

    // Check page arguments
    if (is_array($this->page_arguments)) {
      /* Check if the array is not empty, cannot be on the 
       * first if, because empty() return true if the value is NULL
       * and NULL is a valid for us. Cuz if is NULL, the dev don't put any
       * content on $page_arguments
       */
      if (!empty($this->page_arguments)) {
        $this->item[$this->url]['page arguments'] = $this->page_arguments;
      }
    }

    // Check delivery callback
    if (!empty($this->delivery_callback)) {
      $this->item[$this->url]['delivery callback'] = $this->delivery_callback;
    }

    // Check access callback
    if (!empty($this->access_callback)) {
      $this->item[$this->url]['access callback'] = $this->access_callback;
    }
    
    // Check access arguments
    if (is_array($this->access_arguments)) {
      $this->item[$this->url]['access arguments'] = $this->access_arguments;
    }
    
    // Check theme callback
    if (!empty($this->theme_callback)) {
      $this->item[$this->url]['theme callback'] = $this->theme_callback;
    }
    
    // Check theme arguments
    if (!empty($this->theme_arguments)) {
      $this->item[$this->url]['theme arguments'] = $this->theme_arguments;
    }
    
    // Check file
    if (!empty($this->file)) {
      $this->item[$this->url]['file'] = $this->file;
    }
    
    // Check file path
    if (!empty($this->file_path)) {
      $this->item[$this->url]['file path'] = $this->file_path;
    }
    
    // Check load arguments
    if (!empty($this->load_arguments)) {
      $this->item[$this->url]['load arguments'] = $this->load_arguments;
    }
    
    // Check weight
    if (!empty($this->weight) && is_numeric($this->weight)) {
      $this->item[$this->url]['weight'] = $this->weight;
    }
    
    // Check menu_name
    if (!empty($this->menu_name)) {
      $this->item[$this->url]['menu_name'] = $this->menu_name;
    }
    
    // Check expanded
    if (!empty($this->expanded)) {
      $this->item[$this->url]['expanded'] = $this->expanded;
    }
    
    // Check context
    if (!empty($this->context)) {
      $this->item[$this->url]['context'] = $this->expanded;
    }
    
    // Check tab_parent
    if (!empty($this->tab_parent)) {
      $this->item[$this->url]['tab_parent'] = $this->tab_parent;
    }
    
    // Check tab_root
    if (!empty($this->tab_root)) {
      $this->item[$this->url]['tab_root'] = $this->tab_root;
    }
    
    // check position
    if (!empty($this->position)) {
      $this->item[$this->url]['position'] = $this->position;
    }
    
    // Check type
    if (!empty($this->type)) {
      $this->item[$this->url]['type'] = $this->type;
    }
    
    // Check options
    if (!empty($this->options)) {
      $this->item[$this->url]['options'] = $this->options;
    }
  }

  // Getters and setters
  public function getItem() {
    $this->render();
    return $this->item;
  }

  public function getUrl() {
    return $this->url;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getAccessCallback() {
    return $this->access_callback;
  }

  public function getPageArguments() {
    return $this->page_arguments;
  }

  public function getPageCallback() {
    return $this->page_callback;
  }

  public function getThemeCallback() {
    return $this->theme_callback;
  }

  public function getThemeArguments() {
    return $this->theme_arguments;
  }

  public function getFile() {
    return $this->file;
  }

  public function getFilePath() {
    return $this->file_path;
  }

  public function getWeight() {
    return $this->weight;
  }

  public function getMenuName() {
    return $this->menu_name;
  }

  public function getExpanded() {
    return $this->expanded;
  }

  public function getContext() {
    return $this->context;
  }

  public function getPosition() {
    return $this->position;
  }

  public function getTabParent() {
    return $this->tab_parent;
  }

  public function getTabRoot() {
    return $this->tab_root;
  }

  public function getType() {
    return $this->type;
  }

  public function getOptions() {
    return $this->options;
  }

  public function getTitleCallback() {
    return $this->title_callback;
  }

  public function getTitleArguments() {
    return $this->title_arguments;
  }
  
  public function getDeliveryCallback() {
    return $this->delivery_callback;
  }

  public function getAccessArguments() {
    return $this->access_arguments;
  }
  
  public function getLoadArguments() {
    return $this->load_arguments;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function setDescription($description) {
    $this->description = $description;
  }

  public function setAccessCallback($access_callback) {
    $this->access_callback = $access_callback;
  }

  public function setPageArguments($page_arguments) {
    $this->page_arguments = $page_arguments;
  }

  public function setPageCallback($page_callback) {
    $this->page_callback = $page_callback;
  }

  public function setThemeCallback($theme_callback) {
    $this->theme_callback = $theme_callback;
  }

  public function setThemeArguments($theme_arguments) {
    $this->theme_arguments = $theme_arguments;
  }

  public function setFile($file) {
    $this->file = $file;
  }

  public function setFilePath($file_path) {
    $this->file_path = $file_path;
  }

  public function setWeight($weight) {
    $this->weight = $weight;
  }

  public function setMenuName($menu_name) {
    $this->menu_name = $menu_name;
  }

  public function setExpanded($expanded) {
    $this->expanded = $expanded;
  }

  public function setContext($context) {
    $this->context = $context;
  }

  public function setPosition($position) {
    $this->position = $position;
  }

  public function setTabParent($tab_parent) {
    $this->tab_parent = $tab_parent;
  }

  public function setTabRoot($tab_root) {
    $this->tab_root = $tab_root;
  }

  public function setType($type) {
    $this->type = $type;
  }

  public function setOptions($options) {
    $this->options = $options;
  }

  public function setTitleCallback($title_callback) {
    $this->title_callback = $title_callback;
  }

  public function setTitleArguments($title_arguments) {
    $this->title_arguments = $title_arguments;
  }
  
  public function setDeliveryCallback($delivery_callback) {
    $this->delivery_callback = $delivery_callback;
  }

  public function setAccessArguments($access_arguments) {
    $this->access_arguments = $access_arguments;
  }
  
  public function setLoadArguments($load_arguments) {
    $this->load_arguments = $load_arguments;
  }



}
