<?php

/**
 * Description of Menus
 * This classes represents a container, havinf inside all 'Menu' objects
 * hook_menu (https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7)
 * @author vitorn
 */

class Menus {
  
  // Properties
  private $items;
  private $drupal_menu_array;
  
  // Methods
  public function __construct() {
    $this->items = array();
  }
  
  public function addMenu(Menu $menu) {
    // Add the menu on the items array
    array_push($this->items, $menu);
  }
  
  public function getAllItems() {
    /** Loop for all Menu items, and call the method getItem
     * This method will return the menu array of drupal
     */
    $this->drupal_menu_array = array();
    
    foreach($this->items as $item) {
      $this->drupal_menu_array += $item->getItem();
    }
    
    // return the Array with all menus
    return $this->drupal_menu_array;
  }
}
